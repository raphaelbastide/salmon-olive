# Salmon Olive

A HTML color sorter.

https://raphaelbastide.com/salmon-olive

# Contribute

Do not hesitate to open an issue for more functions, or less bugs.

# Licence

[AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html)
