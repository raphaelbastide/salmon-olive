let d = document, b = d.body, m = b.querySelector('main')
let originalH1 = b.querySelector('h1').innerText
let originalTitle = d.title

displayColors(colors)

function sort(type){
  if (type == 'nameLength') {
    colors.sort((a, b) => {
      return a.cNameLength - b.cNameLength;
    });
  }else if(type == 'hue'){
    colors.sort((a, b) => {
      return a.hue - b.hue;
    });
  }else if(type == 'sat'){
    colors.sort((a, b) => {
      return a.sat - b.sat;
    });
  }else if(type == 'lum'){
    colors.sort((a, b) => {
      return a.lum - b.lum;
    });
  }else if(type == 'nameAlpha'){
    colors.sort((a, b) => {
      let fa = a.name.toLowerCase(),
          fb = b.name.toLowerCase();
      if (fa < fb){return -1;}
      if (fa > fb){return 1;}
      return 0;
    });
  }
  m.innerHTML = ""
  displayColors(colors)
}

function layout(type){
  b.setAttribute('data-layout', type)
}

function displayColors(colors){
  for (var i = 0; i < colors.length; i++) {
    let c = colors[i]
    let cName = c.name
    let cNameLength = cName.length
    c.hue = rgbToHsl(c.red, c.green, c.blue)[0]
    c.sat = rgbToHsl(c.red, c.green, c.blue)[1]
    c.lum = rgbToHsl(c.red, c.green, c.blue)[2]
    c.cNameLength = cNameLength
    let colBox = d.createElement('div')
    colBox.style.backgroundColor = cName
    colBox.innerHTML = "<span class='name copy'>"+cName+"</span>"
    colBox.innerHTML += "<span class='rgb copy'>rgb("+c.red+","+c.green+","+c.blue+")</span>"
    colBox.innerHTML += "<span class='hsl copy'>hsl("+c.hue+","+c.sat+"%,"+c.lum+"%)</span>"
    colBox.innerHTML += "<span class='hex copy'>"+c.hex+"</span>"
    colBox.onclick = function(){
      this.classList.toggle('selected')
    }
    m.appendChild(colBox)
    refreshCopy()
  }
}

function refreshCopy(){
  let canCopy = d.querySelectorAll('.copy')
  for (var i = 0; i < canCopy.length; i++) {
    canCopy[i].onclick = function(){
      let text = this.innerText
      let textArea = document.createElement("textarea");
      textArea.value = text;
      textArea.style.display = "hidden"
      document.body.appendChild(textArea);
      textArea.select();
      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        b.querySelector('h1').innerText = "Copied: "+text
        d.title = "✔ Copied"
        console.log('✔ Copied');
        setTimeout(function () {
          b.querySelector('h1').innerText = originalH1
          d.title = originalTitle
        }, 1000);
      } catch (err) {
        console.log('Oops, unable to copy');
      }
      document.body.removeChild(textArea);
    }
  }
}

function rgbToHsl(r, g, b){
  r /= 255, g /= 255, b /= 255;
  var max = Math.max(r, g, b), min = Math.min(r, g, b);
  var h, s, l = (max + min) / 2;
  if(max == min){
      h = s = 0; // achromatic
  }else{
    var d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
    switch(max){
      case r: h = (g - b) / d + (g < b ? 6 : 0); break;
      case g: h = (b - r) / d + 2; break;
      case b: h = (r - g) / d + 4; break;
    }
    h /= 6;
  }
  return [Math.floor(h * 360), Math.floor(s * 100), Math.floor(l * 100)];
}

function copy(text) {
  var copyText = document.getElementById("myInput");
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */
  document.execCommand("copy");
  alert("Copied the text: " + text);
}
